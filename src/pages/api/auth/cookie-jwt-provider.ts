import { JWT } from "next-auth/jwt";
import { setCookie, parseCookies } from "nookies";

export class CookieJWTProvider implements JWT {
  async getToken(options : any): Promise<null | { [key: string]: any }> {
    const cookies = parseCookies(options);
    const token = cookies["access_token"];
    return token ? JSON.parse(token) : null;
  }

  async setToken(token: { [key: string]: any }, options: any): Promise<void> {
    setCookie(options, "access_token", JSON.stringify(token), {
      maxAge: 30 * 24 * 60 * 60, // 30 days
      path: "/",
    });
  }

  [key: string]: any; // Add index signature
}
