import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { api } from "../../../../config";

async function refreshAccessToken(tokenObject: any) {
  try {
    // Get a new set of tokens with a refreshToken
    // const tokenResponse = await axios.get(api + "/auth/refresh", {
    //   token: tokenObject.refreshToken,
    // });
    const tokenResponse = await fetch(
      api + `/auth/refresh?username=${tokenObject.user.username}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${tokenObject.refreshToken}`,
          "Content-Type": "application/json",
        },
      }
    );

    const data = await tokenResponse.json();

    return {
      ...tokenObject,
      accessToken: data.accessToken,
      accessTokenExpiration: data.accessTokenExpiration,
      refreshToken: data.refreshToken,
    };
  } catch (error) {
    console.log({ error });
    return {
      ...tokenObject,
      error: "RefreshAccessTokenError",
    };
  }
}

export const authOptions = {
  providers: [
    CredentialsProvider({
      name: "Sign in",
      // generate a suitable form on the sign in page.
      credentials: {
        username: {
          label: "Email",
          type: "email",
          placeholder: "hello@example.com",
        },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials) {
        // Request to sign in
        const response = await fetch(api + "/auth/login", {
          method: "POST",
          body: JSON.stringify(credentials),
          headers: { "Content-Type": "application/json" },
        });

        const data = await response.json();
        const tokens = data.tokens;

        // After sign-in, request data user to create session with a complete profile
        const userRequest = await fetch(api + "/users/me", {
          method: "GET",
          headers: {
            Authorization: `Bearer ${tokens.accessToken}`,
            "Content-Type": "application/json",
          },
        });

        const userData = await userRequest.json();

        if (userRequest.statusText === "OK") {
          const user = { ...userData, ...tokens };
          return user;
        }

        // If no error and we have user data, return it

        // return test user
        // return { id: 1, name: 'J Smith', email: 'jsmith@example.com' };

        // Return null if user data could not be retrieved
        return null;
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user }: any) {
      if (user) {
        const {
          accessToken,
          accessTokenExpiration,
          refreshToken,
          ...rest
        } = user;
        token.accessToken = accessToken;
        token.role = user.role;
        token.refreshToken = refreshToken;
        token.accessTokenExpiration = accessTokenExpiration;
        token.user = rest;
      }
      // If accessTokenExpiration is 24 hours, we have to refresh token before 24 hours pass.
      const shouldRefreshTime = Math.round(
        token.accessTokenExpiration - 60 * 60 * 1000 - Date.now()
      );

      // If the token is still valid, just return it.
      if (shouldRefreshTime > 0) {
        return Promise.resolve(token);
      }
      // If the call arrives after 23 hours have passed, we allow to refresh the token.

      token = refreshAccessToken(token);
      return Promise.resolve(token);
    },
    // Extending session object
    async session({ session, token }: any) {
      session.user = {
        ...token.user,
      };
      session.accessToken = token.accessToken;
      session.accessTokenExpiration = token.accessTokenExpiration;
      if (token.error) {
        session.error = token.error;
      }
      // session.error = token.error || undefined;
      return Promise.resolve(session);
    },
  },
  // callbacks: {
  //   async jwt({ token, user }: any) {
  //     return { ...token, ...user };
  //   },
  //   async session({ session, token, user }: any) {
  //     // Send properties to the client, like an access_token from a provider.
  //     session.user = token;

  //     return session;
  //   },
  // },

  pages: {
    signIn: "/account/inloggen",
  },
  debug: true,
  successRedirect: "/",
};

const Auth = (req: NextApiRequest, res: NextApiResponse) =>
  NextAuth(req, res, authOptions);
export default Auth;
