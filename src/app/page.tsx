import React from "react";

import { Recipe } from "../api/types";
import { getRecipes } from "../api/recipes/fetchFunctions";

import HeroImage from "../../public/images/test.png";
import Hero from "../components/Hero";
import RecipeList from "../components/RecipeList";

const HomePage = async () => {
  // Call the API to retrieve recipes and wait for response
  const recipes: Recipe[] = await getRecipes();
  return (
    <div className="min-h-screen">
      <Hero
        image={HeroImage}
        title={"Mijn Maaltijd"}
        text={"Makkelijke recepten voor iedereen"}
      />
      <div className="container mx-auto px-4">
        {/* Render a RecipeList component with the recipes fetched from the API */}
        <RecipeList recipes={recipes} />
      </div>
    </div>
  );
};

export default HomePage;
