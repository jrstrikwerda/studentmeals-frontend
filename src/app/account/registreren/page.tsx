"use client";
import { signIn, useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FormEvent, useRef, useState } from "react";
import ButtonLogin from "../../../components/common/ButtonLogin";
import TextboxLogin from "../../../components/common/TextboxLogin";
import Breadcrumb from "../../../components/common/Breadcrumb";
import { signup } from "../../../api/account/register";

interface IProps {
  searchParams?: { [key: string]: string | string[] | undefined };
}

const RegisterPage = ({ searchParams }: IProps) => {
  const [errorText, setErrorText] = useState("");
  const { data: session } = useSession();
  const router = useRouter();
  if (session) {
    router.push(`/`);
  }
  const firstName = useRef("");
  const lastName = useRef("");
  const email = useRef("");
  const password = useRef("");

  const breadcrumbItems = [
    { label: "Home", href: "/" },
    { label: "Registreren", href: "#" },
  ];

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();

    const res = await signup({
      firstName: firstName.current,
      lastName: lastName.current,
      email: email.current,
      password: password.current,
    });

    if (res.error) {
      setErrorText(res.error);
    } else {
      signIn("credentials", {
        username: email.current,
        password: password.current,
        redirect: true,
        callbackUrl: "/",
      });
    }
  };

  return (
    <div className="container mx-auto px-4">
      {errorText && (
        <p className="text-white bg-chili-darkest mt-1 py-2 px-5 rounded-md">
          {errorText}
        </p>
      )}
      <Breadcrumb items={breadcrumbItems} className="my-4" />

      <div className="relative my-20 py-2 sm:max-w-xl sm:mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-primary-light to-primary-lighter transform -skew-y-2 sm:skew-y-0  sm:-rotate-6 rounded-md sm:rounded-3xl shadow-md shadow-dark hidden sm:block"></div>
        <div className="relative z-10 px-4 pt-6 pb-8 sm:p-12 bg-primary-dark rounded-md sm:rounded-3xl shadow-lg shadow-dark">
          {searchParams?.message && (
            <p className="text-tomato bg-chili-darkest py-2 px-5 rounded-md">
              {searchParams?.message}
            </p>
          )}
          <form className="flex flex-col text-light-lighter">
            <h3 className="mb-6 text-center">Welkom!</h3>
            <div className="mb-12">
              <p className="text-lg ">
                Maak een account aan en sla je favoriete recepten op, beheer je
                profiel of voeg je eigen recepten toe.
              </p>
              <Link
                className={"text-lg underline text-light-light/80 m-0"}
                href={"/account/inloggen"}
              >
                Heb je al een account? Log dan in!
              </Link>
            </div>
            <div className="mb-6 flex gap-6 justify-between flex-col sm:flex-row">
              <TextboxLogin
                lableText="Voornaam *"
                placeholder="John"
                onChange={(e) => (firstName.current = e.target.value)}
                className={"w-full sm:w-2/5"}
              />
              <TextboxLogin
                lableText="Achternaam *"
                placeholder="Doe"
                onChange={(e) => (lastName.current = e.target.value)}
                className={"w-full sm:w-3/5"}
              />
            </div>
            <div className="mb-6">
              <TextboxLogin
                lableText="Email *"
                placeholder="Vul hier jouw e-mailadres in"
                onChange={(e) => (email.current = e.target.value)}
              />
            </div>
            <div className="mb-6">
              <TextboxLogin
                lableText="Wachtwoord *"
                placeholder="Vul hier jouw wachtwoord in"
                onChange={(e) => (password.current = e.target.value)}
                type={"password"}
              />
            </div>
            <div className="flex justify-center w-full">
              <ButtonLogin
                onClick={onSubmit}
                variant={"outline-primary"}
                className={"px-12"}
              >
                Registreren
              </ButtonLogin>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
