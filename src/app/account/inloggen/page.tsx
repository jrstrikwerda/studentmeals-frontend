"use client";
import { signIn, useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useRef } from "react";
import ButtonLogin from "../../../components/common/ButtonLogin";
import TextboxLogin from "../../../components/common/TextboxLogin";
import Breadcrumb from "../../../components/common/Breadcrumb";

interface IProps {
  searchParams?: { [key: string]: string | string[] | undefined };
}

const LoginPage = ({ searchParams }: IProps) => {
  const { data: session } = useSession();
  const router = useRouter();
  if (session) {
    router.push(`/`);
  }
  const userName = useRef("");
  const pass = useRef("");

  const breadcrumbItems = [
    { label: "Home", href: "/" },
    { label: "Inloggen", href: "#" },
  ];

  const onSubmit = async () => {
    const result = await signIn("credentials", {
      username: userName.current,
      password: pass.current,
      redirect: true,
      callbackUrl: "/",
    });
  };

  return (
    <div className="container mx-auto px-4">
      <Breadcrumb items={breadcrumbItems} className="my-4" />
      <div className="relative my-20 py-2 sm:max-w-xl sm:mx-auto">
        <div className="absolute inset-0 bg-gradient-to-r from-primary-light to-primary-lighter transform -skew-y-2 sm:skew-y-0  sm:-rotate-6 rounded-md sm:rounded-3xl shadow-md shadow-dark hidden sm:block"></div>
        <div className="relative z-10 px-4 pt-6 pb-8 sm:p-12 bg-green rounded-md sm:rounded-3xl shadow-lg shadow-dark">
          {searchParams?.message && (
            <p className="text-tomato bg-chili-darkest py-2 px-5 rounded-md">
              {searchParams?.message}
            </p>
          )}
          <form className="flex flex-col text-yellow">
            <h3 className="mb-6 text-center">Welkom terug!</h3>
            <div className="mb-12">
              <p className="text-lg text-yellow-light">
                Log in en bekijk je favoriete recepten, beheer je profiel of
                voeg je eigen recept toe.
              </p>
              <Link
                className={"text-lg underline text-yellow-light/80 m-0"}
                href={"/account/registreren"}
              >
                Nog geen account? Maak er een aan.
              </Link>
            </div>
            <div className="mb-6">
              <TextboxLogin
                lableText="Email *"
                placeholder="Vul hier jouw e-mailadres in"
                onChange={(e) => (userName.current = e.target.value)}
              />
            </div>
            <div className="mb-8">
              <TextboxLogin
                lableText="Wachtwoord *"
                placeholder="Vul hier jouw wachtwoord in"
                type={"password"}
                onChange={(e) => (pass.current = e.target.value)}
              />
            </div>
            <div className="flex justify-center w-full">
              <ButtonLogin
                onClick={onSubmit}
                variant={"outline-yellow"}
                className={"px-12"}
              >
                Inloggen
              </ButtonLogin>
            </div>
            <p className="mt-8 text-sm text-center text-yellow-light/80">
              Wachtwoord vergeten?{" "}
              <Link href={"/account/vergeten"} className={"underline text-sm"}>
                Klik hier
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
