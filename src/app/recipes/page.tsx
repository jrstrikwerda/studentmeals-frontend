import React from "react";
import SmallRecipeList from "../../components/SmallRecipeList";
import Breadcrumb from "../../components/common/Breadcrumb";
import { getRecipes } from "../../api/recipes/fetchFunctions";
import { Recipe } from "../../api/types";



const RecipesPage = async () => {
  const recipes: Recipe[] = await getRecipes();
  const breadcrumbItems = [
    { label: "Home", href: "/" },
    { label: "Recipes", href: "#" },
  ];
  return (
    <div className="min-h-screen">
      {/* <Meta /> */}
      <div className="container mx-auto px-4">
        <Breadcrumb items={breadcrumbItems} className="my-4" />
        <SmallRecipeList recipes={recipes} />
      </div>
    </div>
  );
};

export default RecipesPage;
