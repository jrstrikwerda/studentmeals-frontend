import { api } from "../../../../config";
// import Meta from "../../../components/Meta";
import { Recipe } from "../../../api/types";
import RecipeDetail from "../../../components/RecipeDetail2";
import Breadcrumb from "../../../components/common/Breadcrumb";
import { getRecipe } from "../../../api/recipes/fetchFunctions";

type Props = {
  params: {
    id: string;
  };
};

const RecipePage = async ({ params: { id } }: Props) => {
  const recipe = await getRecipe(id.split("-").slice(-1).toString());

  const breadcrumbItems = [
    { label: "Home", href: "/" },
    { label: "Recipes", href: "/recipes" },
    { label: recipe.name, href: "#" },
  ];
  return (
    <div className="">
      {/* <Meta title={recipe.name} description={recipe.description} /> */}
      <div className="container mx-auto px-4">
        <Breadcrumb items={breadcrumbItems} className="my-4" />
      </div>
      <RecipeDetail recipe={recipe} />
    </div>
  );
};

export default RecipePage;
