import "../styles/globals.scss";
// components
import Meta from "../components/common/Meta";
import Nav from "../components/nav/Nav";
import Footer from "../components/footer/Footer";
import React from "react";
import Providers from "../components/utils/Providers";
import { getSession } from "../api/session";

type LayoutProps = {
  children: React.ReactNode;
};

const RootLayout = async ({ children }: LayoutProps) => {
  const session = await getSession();
  return (
    <html lang="nl">
      <Meta />
      <body className="bg-pearl 2xl:bg-pearl">
        <Providers session={session}>
          <div id="site-wrapper" className="font-poppins min-h-screen">
            <Nav session={session}/>
            <main className="w-full relative">
              {children}
            </main>
          </div>
          <Footer />
        </Providers>
      </body>
    </html>
  );
};

export default RootLayout;
