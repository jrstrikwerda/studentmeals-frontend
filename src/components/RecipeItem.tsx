import Link from "next/link";
import React from "react";
import { Recipe } from "../api/types";
import Image from "next/image";

interface CardProps {
  recipe: Recipe;
}

const RecipeItem: React.FC<CardProps> = ({ recipe }) => {
  return (
    <Link
      href={`/recipes/${recipe?.name?.toLowerCase().replaceAll(" ", "-")}-${
        recipe._id
      }`}
      className="flex flex-col justify-between text-left no-underline flex-1 rounded-md shadow-md hover:shadow-xl bg-light-lighter  overflow-hidden hover:scale-[1.01] ease-in duration-300"
    >
      <div>
        <Image
          src={recipe.image}
          alt={recipe.name}
          width={600}
          height={400}
          className="w-full object-cover h-44 lg:h-52 object-center"
          priority
        />
        <div className="flex justify-between mx-4 mt-4">
          <div className="flex bg-yellow-light rounded-md px-2 py-1  items-center">
            <svg
              width={18}
              height={18}
              className="fill-green mr-2"
              viewBox="0 0 32 32"
              xmlns="http://www.w3.org/2000/svg"
              stroke="currentColor"
            >
              <path
                xmlns="http://www.w3.org/2000/svg"
                d="M16 0.75c-8.422 0-15.25 6.828-15.25 15.25s6.828 15.25 15.25 15.25c8.422 0 15.25-6.828 15.25-15.25v0c-0.010-8.418-6.832-15.24-15.249-15.25h-0.001zM16 28.75c-7.042 0-12.75-5.708-12.75-12.75s5.708-12.75 12.75-12.75c7.042 0 12.75 5.708 12.75 12.75v0c-0.008 7.038-5.712 12.742-12.749 12.75h-0.001zM17.25 15.482v-9.482c0-0.69-0.56-1.25-1.25-1.25s-1.25 0.56-1.25 1.25v0 10c0 0.345 0.14 0.658 0.366 0.884l3.999 4.001c0.226 0.226 0.539 0.366 0.884 0.366 0.691 0 1.251-0.56 1.251-1.251 0-0.345-0.14-0.658-0.366-0.884l0 0z"
              />
            </svg>
            <span className="text-primary-darker text-sm font-semibold">
              {recipe.cookTime} min
            </span>
          </div>
          <div className="flex justify-end bg-transparent">
            <div className="focus:outline-none beat">
              <svg
                className="w-6 h-6 hover:animate-beat"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  className="w-6 h-6 text-tomato tertiary fill-pearl hover:fill-tomato"
                  d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"
                  fill="currentColor"
                  stroke="currentColor"
                  strokeWidth="2"
                />
              </svg>
            </div>
          </div>
        </div>
        <div className="mx-4 my-4 mb-10">
          <div className="font-bold text-xl my-2 text-green">
            {recipe.name}
          </div>
          {/* <p className="text-light-light">{recipe.description}</p> */}
        </div>
      </div>
      <div className="relative flex items-end justify-end">
        <div className="absolute bg-yellow border-0 py-2 px-4 focus:outline-none hover:bg-yellow-light ease-in duration-300 rounded-tl-md">
          <svg
            className="w-5 h-5 text-black tertiary fill-light"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M17 8l4 4m0 0l-4 4m4-4H3"
            ></path>
          </svg>
        </div>
      </div>
    </Link>
  );
};

// const RecipeItem: React.FC<CardProps> = ({ recipe }) => (
//   <Link
//     href={`/recipe/${recipe._id}`}
//     as={`/recipes/${recipe._id}`}
//     className="flex flex-col flex-1 max-w-sm relative filter hover:contrast-50 rounded-xl shadow-lg overflow-hidden "
//   >
//     <h3 className="font-bold text-2xl p-2 text-accent-lighter leading-tight truncate bg-tertiary-dark">{recipe.name}</h3>
//     <div className="bg-gray-800 bg-opacity-75 text-white absolute top-0 mt-12 p-2">
//       <p className="text-sm font-semibold leading-tight truncate">
//         {recipe.description}
//       </p>
//     </div>
//     <Image
//       src={recipe.image}
//       alt={recipe.name}
//       width={400}
//       height={300}
//       loading="lazy"
//       className="object-cover w-full"
//     />
//   </Link>
// );

export default RecipeItem;
