import footerStyles from "../styles/components/Footer.module.scss";
import React from "react";

const Footer = () => {
  return (
    <div className="w-full bg-gradient-to-r from-green to-green text-yellow-light ">
      <footer id="footer" className="px-2 sm:px-4 py-2.5">
        <div className="container flex flex-wrap justify-center items-center mx-auto">
          <small className="flex items-center justify-between">&copy; Mijn Maaltijd {new Date().getFullYear()}</small>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
