import Image from 'next/image';
import { Recipe } from '../api/types';
import { RecipeIngredient } from '../api/types';

const RecipeDetail: React.FC<{ recipe: Recipe }> = ({ recipe }) => {
  return (
    <div className={`max-w-3xl mx-auto p-6`}>
      <h1 className={`text-3xl font-bold text-gray-900`}>{recipe.name}</h1>
      <div className={`mt-4`}>
        <Image
          src={recipe.image}
          alt={recipe.name}
          className={`w-full rounded-lg shadow-lg`}
        />
      </div>
      <h2 className={`text-2xl font-bold text-gray-900 mt-6`}>Ingredients</h2>
      <ul className={`mt-4 list-none`}>
        {recipe.recipeIngredients.map((recipeIngredient) => (
          <li
            key={recipeIngredient?.ingredient?.name}
            className={`text-gray-600`}
          >
            {recipeIngredient?.ingredient?.name}
          </li>
        ))}
      </ul>
      <h2 className={`text-2xl font-bold text-gray-900 mt-6`}>Instructions</h2>
      <ol className={`mt-4 list-decimal`}>
        <li key={"Instruction_1"} className={`text-gray-600`}>
          {"Instruction_1"}
        </li>
        <li key={"Instruction_2"} className={`text-gray-600`}>
          {"Instruction_2"}
        </li>
        <li key={"Instruction_3"} className={`text-gray-600`}>
          {"Instruction_3"}
        </li>
        {/* {recipe.instructions.map(instruction => (
          <li key={instruction} className={`text-gray-600`}>
            {instruction}
          </li>
        ))} */}
      </ol>
    </div>
  );
};

export default RecipeDetail;
