import Image, { StaticImageData } from "next/image";
import SearchHero from "./common/SearchHero";

type Props = {
  image: StaticImageData;
  title: string;
  text: string;
};

const Hero = ({ image, title, text }: Props) => (
  <div className="relative flex flex-col items-center justify-center xl:justify-end xl:pb-24 lg:pt-32 w-full max-w-125vw min-h-[40vh] md:min-h-[44vh] lg:min-h-[48vh] xl:min-h-[56vh]">
    <div className="container mx-auto px-4 relative flex flex-col-reverse items-center h-full z-10 py-12 text-center">
      <div className="text-white max-w-2xl px-4">
        {/* <h2 className="text-4xl md:text-6xl font-bold pb-4 text-accent-text shadow-secondary text-shadow">{title}</h2> */}
        <span className="text-4xl md:text-6xl font-semibold pb-4 text-yellow shadow-secondary text-shadow">
           Mijn Maaltijd
        </span>
        <p className="text-lg md:text-xl pb-4 text-light-lighter shadow-dark text-shadow-lg">
          {text}
        </p>
        <SearchHero/>
      </div>
    </div>
    <Image
      className="object-cover mb-5 object-top brightness-75"
      priority
      fill
      src={image}
      alt="hero-image"
    />
  </div>
);

export default Hero;
