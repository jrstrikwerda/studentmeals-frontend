import RecipeItem from "./RecipeItem";
import recipeStyles from "../styles/components/Recipe.module.scss";
import React from 'react';

import { Recipe } from "../api/types";

type Props = {
  recipes: Recipe[];
};

const SmallRecipeList = ({ recipes }: Props)=> {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 gap-6 row-gap-6 my-8">
      {recipes.map((recipe) => {
        return (
          <RecipeItem recipe={recipe} key={recipe._id} />
        );
      })}
    </div>
  );
};

export default SmallRecipeList;
