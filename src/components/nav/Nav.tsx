"use client";
import React, { useState } from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import Tomato from "../../../public/tomato.svg";
import useAuth from "../hooks/useAuth";
import { Session } from "next-auth";
import AccountPopup from "./AccountPopup";
import SearchNavDesktop from "../common/SearchNavDesktop";
import SearchNavMobile from "../common/SearchNavMobile";

type Props = {
  session: Session;
};

const Nav = ({ session }: Props) => {
  const pathname = usePathname();
  const isAuthenticated = useAuth(false);
  const [showMenu, setShowMenu] = useState(false);

  const toggleMobileMenu = () => setShowMenu((prevState) => !prevState);

  // function toggleMobileMenu(dontOpen = false) {
  //   const menu = document.querySelector("#menu");
  //   if (dontOpen) {
  //     if (menu?.classList.contains(`hidden`)) {
  //       menu?.classList.toggle(`hidden`);
  //     }
  //   } else {
  //     menu?.classList.toggle(`hidden`);

  //   }
  // }

  return (
    <div className="w-full  bg-gradient-to-r from-green to-green/95 text-light-lighter">
      <nav className="px-2 sm:px-4 py-2.5" id="navbar">
        <div className="container flex flex-wrap justify-between items-center mx-auto">
          {/* Brand/Logo */}
          <Link href="/" className="flex items-center">
            <Tomato className="h-12 sm:h-12 text-chili-darkest fill-chili-darkest mr-1.5 bg-tomato-inner rounded-full" />

            <span className="self-center text-xl font-semibold whitespace-nowrap text-yellow">
              Mijn Maaltijd
            </span>
          </Link>
          <div className="flex md:order-2">
            {/* Search icon mobile view */}
            <button
              type="button"
              data-collapse-toggle="navbar-search"
              aria-controls="navbar-search"
              aria-expanded="false"
              className="md:hidden text-light-lighter  hover:bg-accent  focus:outline-none focus:ring-2 focus:ring-accent  rounded-lg text-sm p-2.5 mr-1"
              onClick={() => toggleMobileMenu()}
            >
              <svg
                className="w-5 h-5 fill-light"
                aria-hidden="true"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Search</span>
            </button>
            {/* Search input desktop */}
            <SearchNavDesktop />
            {/* Burger menu */}
            <button
              data-collapse-toggle="navbar-search"
              type="button"
              className="inline-flex items-center p-2 text-sm text-light-lighter rounded-lg md:hidden hover:bg-accent focus:outline-none focus:ring-2 focus:ring-accent"
              aria-controls="navbar-search"
              aria-expanded="false"
              onClick={() => toggleMobileMenu()}
            >
              <span className="sr-only">Open menu</span>
              <svg
                className="w-6 h-6 fill-light"
                aria-hidden="true"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </button>
          </div>
          {/* New menu */}

          <div
            className={`${
              showMenu
                ? "opacity-100 h-test sm:h-full"
                : "opacity-0 sm:opacity-100  h-0 sm:h-full"
            } justify-between items-center w-full md:flex md:w-auto md:order-1 transition-all sm:transition-none duration-500 overflow-hidden`}
            id="menu"
          >
            {/* Search input mobile */}
            <SearchNavMobile />
            {/* Menu */}
            <ul className="flex flex-col p-2 mt-4 rounded-lg border border-dark md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-transparent bg-primary-light">
              <li className="nav-item">
                <Link
                  className={
                    (pathname == "/about" ? "underline" : "") +
                    " block py-2 px-4 text-yellow-light rounded hover:bg-primary md:hover:bg-transparent md:hover:text-yellow md:p-0 "
                  }
                  onClick={() => toggleMobileMenu()}
                  href="/about"
                >
                  About
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={
                    (pathname == "/info" ? "underline" : "") +
                    " block py-2 px-4 text-yellow-light rounded hover:bg-primary md:hover:bg-transparent md:hover:text-yellow md:p-0 "
                  }
                  onClick={() => toggleMobileMenu()}
                  href="/info"
                >
                  Info
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className={
                    (pathname == "/recipes" ? "underline" : "") +
                    " block py-2 px-4 text-yellow-light hover:bg-primary md:hover:bg-transparent md:hover:text-yellow md:p-0 sm:border-0 border-b border-light-dark "
                  }
                  onClick={() => toggleMobileMenu()}
                  href="/recipes"
                >
                  Recipes
                </Link>
              </li>

              <AccountPopup
                isAuthenticated={isAuthenticated}
                role={session?.user?.role}
                pathname={pathname}
              />
            </ul>
          </div>
          {/* Menu  */}
        </div>
      </nav>
    </div>
  );
};

export default Nav;
