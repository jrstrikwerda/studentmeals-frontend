import { signOut } from "next-auth/react";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";

type Props = {
  isAuthenticated: boolean;
  role: string | undefined;
  pathname: string | null;
};

function AccountPopup({ isAuthenticated, role, pathname }: Props) {
  const [showPopup, setShowPopup] = useState(false);
  const popupRef = useRef<HTMLDivElement>(null);
  const accountLinkRef = useRef<HTMLAnchorElement>(null);
  const [popupWidth, setPopupWidth] = useState(0);
  const [popupPosition, setPopupPosition] = useState({ x: -9999, y: 0 });
  const popupTimeoutRef = useRef<number | null>(null);

  useEffect(() => {
    if (popupRef.current && accountLinkRef.current) {
      setPopupWidth(popupRef.current.offsetWidth);
      const linkRect = accountLinkRef.current.getBoundingClientRect();
      const linkX = linkRect.left;
      const linkY = linkRect.bottom;
      const linkWidth = linkRect.width;

      // Wait for the popup width to update before calculating the position
      if (popupWidth) {
        const popupHeight = popupRef.current.offsetHeight;
        const popupX = Math.max(0, linkX + linkWidth / 2 - popupWidth / 2);
        const popupY = linkY + 15;

        // Update the state with the new popup position
        setPopupPosition({
          x: popupX,
          y: popupY,
        });
      }
    }
  }, [popupWidth, showPopup]);

  const authLinks = (
    <>
      <Link
        href="/favorieten"
        className="block mt-4 lg:inline-block lg:mt-0 text-light hover:text-dark mr-4"
      >
        Mijn favorieten
      </Link>
      {role === "Admin" && (
        <Link
          href="/account/admin"
          className="block mt-4 lg:inline-block lg:mt-0 text-light hover:text-dark mr-4"
        >
          Recepten aanpassen
        </Link>
      )}
      <Link
        href="/account"
        className="sm:block mt-4 lg:inline-block lg:mt-0 text-light hover:text-dark mr-4 hidden"
      >
        Mijn account
      </Link>
      <a
        className="block mt-4 lg:inline-block lg:mt-0 text-light hover:text-dark"
        onClick={() => signOut()}
      >
        Uitloggen
      </a>
    </>
  );

  const guestLinks = (
    <>
      <p className="text-light-lighter sm:mb-2 sm:max-w-[20ch] sm:not-italic italic text-sm sm:text-base">
        Met een account is het mogelijk je favoriete recepten op te slaan.
      </p>
      <div className="flex sm:justify-end flex-col sm:flex-row">
        <Link
          href="/account/registreren"
          className="block mt-1 sm:mt-2 lg:inline-block lg:mt-0 text-light hover:text-dark mr-4"
        >
          Registreren
        </Link>
        <Link
          href="/account/inloggen"
          className="block mt-1 sm:mt-2  lg:inline-block lg:mt-0 text-light hover:text-dark mr-4"
        >
          Inloggen
        </Link>
      </div>
    </>
  );

  return (
    <li className="nav-item">
      <Link
        ref={accountLinkRef}
        href={isAuthenticated ? "/account" : "/account/inloggen"}
        className={
          (pathname && pathname.includes("account") ? "underline" : "") +
          " block sm:pb-2 pt-2 px-4 text-yellow-light rounded hover:bg-primary md:hover:bg-transparent md:hover:text-yellow md:p-0  "
        }
        onMouseEnter={() => {
          if (popupTimeoutRef.current) {
            clearTimeout(popupTimeoutRef.current);
            popupTimeoutRef.current = null;
          }
          setShowPopup(true);
        }}
        onMouseLeave={() => {
          popupTimeoutRef.current = window.setTimeout(() => {
            setShowPopup(false);
          }, 4000);
        }}
      >
        Mijn account
      </Link>

      <div
        ref={popupRef}
        className={`sm:absolute sm:bg-primary-light rounded-lg sm:shadow-lg p-4  pt-0 sm:pt-4 z-20 transition-all duration-500 ease-in-out ${
          showPopup
            ? "opacity-100 scale-100 translate-y-0"
            : "sm:opacity-0 sm:scale-[.2] sm:-translate-y-10"
        }  `}
        style={{
          left: popupPosition.x,
          top: popupPosition.y,
        }}
        onMouseEnter={() => {
          if (popupTimeoutRef.current) {
            clearTimeout(popupTimeoutRef.current);
            popupTimeoutRef.current = null;
          }
        }}
        onMouseLeave={() => {
          setShowPopup(false);
        }}
      >
        <div className="flex flex-col sm:gap-2">
          {isAuthenticated ? authLinks : guestLinks}{" "}
        </div>
      </div>
    </li>
  );
}
export default AccountPopup;
