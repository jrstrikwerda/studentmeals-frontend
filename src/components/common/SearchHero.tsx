"use client";

// Import required modules
import Link from "next/link";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Recipe } from "../../api/types";
import useLiveSearch from "../hooks/useLiveSearch";
import Tomato from "../../../public/tomato.svg";

// SearchHero component
const SearchHero: React.FC = () => {
  const {
    searchQuery,
    setSearchQuery,
    searchStarted,
    results,
    error,
    loading,
    handleFocus,
    handleBlur,
    showResults,
  } = useLiveSearch();

  // Render search input and results
  return (
    <div className="relative md:mx-9">
      <SearchInput
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
        handleFocus={handleFocus}
        handleBlur={handleBlur}
        loading={loading}
      />
      {error && (
        <div className="bg-tomato text-white p-3 rounded-lg">
          {error.toString()}
        </div>
      )}
      <SearchResults
        results={results}
        query={searchQuery}
        showResults={showResults}
        searchStarted={searchStarted}
      />
    </div>
  );
};

export default SearchHero;

// SearchInput component
interface SearchInputProps {
  searchQuery: string;
  setSearchQuery: (value: string) => void;
  handleFocus: () => void;
  handleBlur: () => void;
  loading: boolean;
}

const SearchInput: React.FC<SearchInputProps> = ({
  searchQuery,
  setSearchQuery,
  handleFocus,
  handleBlur,
  loading,
}) => {
  return (
    <>
      <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
        <SearchIcon />
      </div>
      <input
        className="h-16 px-10 block shadow-md text-md shadow-dark py-2 w-full text-dark bg-pearl placeholder-green-light rounded-lg border border-dark sm:text-lg focus:ring-tertiary focus:border-tertiary "
        type="text"
        value={searchQuery}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setSearchQuery(e.target.value)
        }
        onFocus={handleFocus}
        onBlur={handleBlur}
        placeholder="Search..."
      />
      <div className="flex absolute inset-y-0 items-center pr-3 right-0 pointer-events-none">
        {loading && (
          <Tomato className="animate-spin h-6 w-6 text-chili-darkest fill-chili-darkest bg-tomato-inner rounded-full" />
        )}
      </div>
    </>
  );
};

// SearchIcon component
const SearchIcon: React.FC = () => (
  <svg
    className="w-5 h-5 text-light tertiary fill-dark"
    aria-hidden="true"
    viewBox="0 0 20 20"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
      clipRule="evenodd"
    ></path>
  </svg>
);

// SearchResults component
interface SearchResultsProps {
  results: Recipe[];
  query: string;
  showResults: boolean;
  searchStarted: boolean;
}

const SearchResults: React.FC<SearchResultsProps> = ({
  results,
  query,
  showResults,
  searchStarted,
}) => {
  const [containerHeight, setContainerHeight] = useState<number>(0);

  useEffect(() => {
    calculateContainerHeight(showResults, query, results, setContainerHeight);
  }, [showResults, query, results]);

  return (
    <ul
      className={`results-container bg-primary-light mt-1 absolute left-0 rounded-lg shadow-lg shadow-dark w-full z-10 overflow-y-auto  text-light-lighter scrollbar-thin scrollbar-thumb-rounded scrollbar-thumb-light scrollbar-track-rounded scrollbar-track-primary-light transition-all ease-in-out duration-500 ${
        showResults && query.trim()
          ? "opacity-100 "
          : "opacity-0 pointer-events-none"
      }`}
      style={{ height: `${containerHeight}rem` }}
    >
      {renderSearchResults(results, searchStarted, query)}
    </ul>
  );
};

// Calculate containerHeight function
function calculateContainerHeight(
  showResults: boolean,
  query: string,
  results: Recipe[],
  setContainerHeight: {
    (value: React.SetStateAction<number>): void;
    (arg0: number): void;
  }
) {
  if (showResults && query.trim() && results.length > 0) {
    // Use the scrollHeight of the container to set its height to animate the transition
    setContainerHeight((prevHeight: number) => {
      const newHeight = Math.min(15 * 16, (results.length * 40) / 16) + 0.1;
      return newHeight !== prevHeight ? newHeight : prevHeight;
    });
  } else if (showResults && query.trim() && results.length === 0) {
    // Calculate the height of the search results container to include the height of the message
    const messageHeight = 65 / 16; // Height of each message item (px)
    setContainerHeight(messageHeight + 0.1);
  } else {
    setContainerHeight(0);
  }
}

// Render search results function
function renderSearchResults(
  results: Recipe[],
  searchStarted: boolean,
  query: string
) {
  return (
    <div>
      {results.length === 0 && searchStarted && (
        <li
          className={`px-5 py-2 text-base 
        `}
        >
          <p className="">Geen recepten gevonden voor:</p>
          <p className="truncate italic text-light">{query}</p>
        </li>
      )}

      {results.length > 0 &&
        results.map((recipe: Recipe, index: number) => (
          <li
            key={recipe._id}
            className={
              "border-light " +
              (index === results.length - 1 ? "border-0" : "border-b")
            }
          >
            <Link
              href={`/recipes/${recipe?.name
                ?.toLowerCase()
                .replaceAll(" ", "-")}-${recipe._id}`}
              className="block px-5 py-2 hover:bg-primary text-base"
            >
              {recipe?.name}
            </Link>
          </li>
        ))}
    </div>
  );
}
