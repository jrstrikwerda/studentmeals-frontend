import React, { useState } from 'react';

interface ModalProps {
  trigger: React.ReactNode;
  children: React.ReactNode;
  className?: string;
}

const Modal: React.FunctionComponent<ModalProps> = ({ trigger, children, className }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleModal = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      {trigger && (
        <button onClick={toggleModal}>
          {trigger}
        </button>
      )}
      {isOpen && (
        <div className={`fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 ${className}`}>
          <button
            className="absolute top-0 right-0 px-4 py-2 text-sm font-bold text-white bg-transparent rounded-full"
            onClick={toggleModal}
          >
            Close
          </button>
          <div className="px-4 py-2 mx-auto max-w-sm bg-light-light rounded-md shadow-md">
            {children}
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
