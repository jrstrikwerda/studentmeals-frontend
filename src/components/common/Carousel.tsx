import Image from "next/image";
import { useState } from "react";

interface CarouselItem {
  title?: string;
  content: React.ReactNode;
  imageUrl: string;
}

interface CarouselProps {
  items: CarouselItem[];
  className?: string;
}

const Carousel: React.FunctionComponent<CarouselProps> = ({
  items,
  className,
}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const handlePrevClick = () => {
    setActiveIndex((activeIndex - 1 + items.length) % items.length);
  };

  const handleNextClick = () => {
    setActiveIndex((activeIndex + 1) % items.length);
  };

  return (
    <div className={`relative ${className}`}>
      <button
        className="absolute inset-y-0 left-0 w-12 h-full flex items-center justify-center rounded-l-lg focus:outline-none focus:shadow-outline-blue"
        onClick={handlePrevClick}
      >
        <svg
          className="h-6 w-6 text-gray-500"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M15 19l-7-7 7-7"
          />
        </svg>
      </button>
      <div className="relative w-full overflow-hidden">
        <Image
          className="w-full h-64 object-cover"
          src={items[activeIndex].imageUrl}
          alt={items[activeIndex].title || "image"}
          width={800}
          height={600}
        />
        <div className="absolute inset-x-0 bottom-0 w-full px-4 py-6 bg-gray-800 bg-opacity-50">
          {items[activeIndex].title && (
            <h2 className="text-2xl font-bold text-white mb-2">
              {items[activeIndex].title}
            </h2>
          )}
          <p className="text-lg text-gray-300">{items[activeIndex].content}</p>
        </div>
      </div>
      <button
        className="absolute inset-y-0 right-0 w-12 h-full flex items-center justify-center rounded-r-lg focus:outline-none focus:shadow-outline-blue"
        onClick={handleNextClick}
      >
        <svg
          className="h-6 w-6 text-gray-500"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M9 5l7 7-7 7"
          />
        </svg>
      </button>
    </div>
  );
};

export default Carousel;
