import React from 'react';

interface ProgressBarProps {
  value: number;
  max: number;
  className?: string;
}

const ProgressBar: React.FunctionComponent<ProgressBarProps> = ({ value, max, className }) => {
  const width = (value / max) * 100;

  return (
    <div className={`relative h-4 ${className}`}>
      <div
        className="absolute inset-0 flex items-center"
      >
        <div
          className="w-full bg-gray-300"
        />
      </div>
      <div
        style={{ width: `${width}%` }}
        className="relative flex items-center text-xs leading-5 font-medium text-white bg-teal-500"
      >
        {value}/{max}
      </div>
    </div>
  );
};

export default ProgressBar;