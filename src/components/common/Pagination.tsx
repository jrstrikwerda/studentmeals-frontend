import React from 'react';
import Link from 'next/link';

interface PaginationProps {
  currentPage: number;
  totalPages: number;
  className?: string;
}

const Pagination: React.FunctionComponent<PaginationProps> = ({ currentPage, totalPages, className }) => {
  const isFirstPage = currentPage === 1;
  const isLastPage = currentPage === totalPages;

  return (
    <nav className={`${className}`}>
      <Link href={`?page=${currentPage - 1}`}>
        <a className={`${isFirstPage ? 'hidden' : ''} px-2 py-1 rounded-md text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 focus:outline-none focus:text-gray-800`}>
          Previous
        </a>
      </Link>
      <Link href={`?page=${currentPage + 1}`}>
        <a className={`${isLastPage ? 'hidden' : ''} px-2 py-1 rounded-md text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 focus:outline-none focus:text-gray-800`}>
          Next
        </a>
      </Link>
    </nav>
  );
};

export default Pagination;