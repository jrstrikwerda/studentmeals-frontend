import React, { useState } from 'react';

interface AccordionItem {
  title: string;
  content: React.ReactNode;
}

interface AccordionProps {
  items: AccordionItem[];
  className?: string;
}

const Accordion: React.FunctionComponent<AccordionProps> = ({ items, className }) => {
  const [activeItem, setActiveItem] = useState<number | null>(null);

  return (
    <div className={`${className}`}>
      {items.map((item, index) => (
        <div key={item.title}>
          <button
            className="block w-full px-4 py-2 font-bold text-left text-sm leading-5 text-gray-700 hover:text-gray-900 focus:outline-none focus:text-gray-900"
            onClick={() => setActiveItem(index === activeItem ? null : index)}
          >
            <span className="inline-block mr-3">{item.title}</span>
            <span className="inline-block w-4 h-4 transform transition-transform duration-200">
              {index === activeItem ? '-' : '+'}
            </span>
          </button>
          {index === activeItem && (
            <div className="px-4 py-3">
              {item.content}
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default Accordion;
