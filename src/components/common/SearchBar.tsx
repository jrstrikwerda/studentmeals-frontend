import React, { FormEvent, useState } from 'react';
import { useRouter } from 'next/navigation';

const SearchBar: React.FC = () => {
  const router = useRouter();
  const [query, setQuery] = useState('');

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    router.push(`/search?query=${query}`);
  };

  return (
    <form onSubmit={handleSubmit} className="w-full max-w-lg mt-4 mx-auto">
      <div className="flex items-center border-b-2 border-teal-500 py-2">
        <input
          type="text"
          placeholder="Search for recipes..."
          value={query}
          onChange={event => setQuery(event.target.value)}
          className="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
        />
        <button type="submit" className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded">
          Search
        </button>
      </div>
    </form>
  );
};