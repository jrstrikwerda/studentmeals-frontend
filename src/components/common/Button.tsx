import Link from "next/link";

interface ButtonProps {
  text: string | JSX.Element;
  url: string;
  className?: string;
}

const Button = ({ text, url, className }: ButtonProps) => {
  return (
    <Link
      href={url}
      className={`px-4 py-2 rounded-md text-base font-medium  ease-in duration-300 ${className}`}
    >
      {text}
    </Link>
  );
};

export default Button;
