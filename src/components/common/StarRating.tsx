"use client"

import React from 'react';

interface StarRatingProps {
  rating: number
  numberRatings: number
  onClick?: (newRating: number) => void
}

const StarRating: React.FC<StarRatingProps> = ({ rating, numberRatings, onClick }) => {
  const stars = [];
  for(let i = 1; i <= 5; i++) {
    stars.push(
      <svg key={i}
        onClick={() => onClick && onClick(i)}
        width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.62L12 2L9.19 8.62L2 9.24L7.45 13.97L5.82 21L12 17.27Z"
          fill={i <= rating ? '#ffc107' : '#2E604C'}
        />
      </svg>
    );
  }

  return (
    <div className="flex justify-center items-center">
      {stars} <p className="text-primary-darker font-medium ml-2 align-middle">({numberRatings})</p>
    </div>
  )
}

export default StarRating;
