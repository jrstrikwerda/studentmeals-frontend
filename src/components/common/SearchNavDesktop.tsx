"use client";

import Link from "next/link";
import React from "react";
import { Recipe } from "../../api/types";
import useLiveSearch from "../hooks/useLiveSearch";
import Tomato from "../../../public/tomato.svg";

const SearchNavDesktop: React.FC = () => {
  const {
    searchQuery,
    setSearchQuery,
    searchStarted,
    results,
    error,
    loading,
    handleFocus,
    handleBlur,
    showResults,
  } = useLiveSearch();

  return (
    <div className="hidden relative md:block">
      <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
        <svg
          className="w-5 h-5 text-tertiary tertiary fill-light-lighter z-10"
          aria-hidden="true"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule="evenodd"
          ></path>
        </svg>
        <span className="sr-only">Search icon</span>
      </div>
      <input
        className="px-5 text-base block p-2 pl-10 w-[35ch] placeholder-light-lighter rounded-xl sm:text-sm outline-tertiary focus:ring-tertiary focus:border-tertiary bg-green-light text-light-lighter hover:bg-primary-lighter focus:bg-primary-lighter ease-in duration-300"
        type="text"
        value={searchQuery}
        id="search-navbar-desktop"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setSearchQuery(e.target.value)
        }
        onFocus={handleFocus}
        onBlur={handleBlur}
        placeholder="Search..."
      />
      <div className="flex absolute inset-y-0 items-center pr-3 right-0 pointer-events-none">
        {" "}
        {loading && (
          <Tomato className="animate-spin h-6 w-6 text-chili-darkest fill-chili-darkest bg-tomato-inner rounded-full" />
        )}
      </div>
      {error && (
        <div className="bg-tomato text-white p-3 rounded-lg">
          {error.toString()}
        </div>
      )}
      <SearchResults
        results={results}
        query={searchQuery}
        showResults={showResults}
        searchStarted={searchStarted}
      />
    </div>
  );
};

interface Props {
  results: Recipe[];
  query: string;
  showResults: boolean;
  searchStarted: boolean;
}

const SearchResults: React.FC<Props> = ({
  results,
  query,
  showResults,
  searchStarted,
}) => {
  const [containerHeight, setContainerHeight] = React.useState<number>(0);

  React.useEffect(() => {
    if (showResults && query.trim() && results.length > 0) {
      // Use the scrollHeight of the container to set its height to animate the transition
      setContainerHeight((prevHeight) => {
        const newHeight = Math.min(15 * 16, (results.length * 40) / 16) + 0.1;
        return newHeight !== prevHeight ? newHeight : prevHeight;
      });
    } else if (showResults && query.trim() && results.length === 0) {
      // Calculate the height of the search results container to include the height of the message
      const messageHeight = 65 / 16; // Height of each message item (px)
      setContainerHeight(messageHeight + 0.1);
    } else {
      setContainerHeight(0);
    }
  }, [showResults, query, results]);
  return (
    <ul
      className={`results-container bg-primary-light mt-1 absolute left-0 rounded-lg shadow-lg shadow-dark w-full z-10 overflow-y-auto  text-light-lighter scrollbar-thin scrollbar-thumb-rounded scrollbar-thumb-light scrollbar-track-rounded scrollbar-track-primary-light transition-all ease-in-out duration-500 ${
        showResults && query.trim()
          ? "opacity-100 "
          : "opacity-0 pointer-events-none"
      }`}
      style={{ height: `${containerHeight}rem` }}
    >
      {results.length === 0 && searchStarted && (
        <li
          className={`px-5 py-2 text-base 
            `}
        >
          <p className="">Geen recepten gevonden voor:</p>
          <p className="truncate italic text-light">{query}</p>
        </li>
      )}
      {results.length > 0 &&
        results.map((recipe, index) => (
          <li
            key={recipe._id}
            className={
              "border-light " +
              (index === results.length - 1 ? "border-0" : "border-b")
            }
          >
            <Link
              href={`/recipes/${recipe?.name
                ?.toLowerCase()
                .replaceAll(" ", "-")}-${recipe._id}`}
              className="block px-5 py-2 hover:bg-primary text-base"
            >
              {recipe?.name}
            </Link>
          </li>
        ))}
    </ul>
  );
};

export default SearchNavDesktop;
