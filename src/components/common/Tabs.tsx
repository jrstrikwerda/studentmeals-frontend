import React, { useState } from 'react';

interface Tab {
  label: string;
  content: React.ReactNode;
}

interface TabsProps {
  tabs: Tab[];
  className?: string;
}

const Tabs: React.FunctionComponent<TabsProps> = ({ tabs, className }) => {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className={`${className}`}>
      <ul className="flex border-b">
        {tabs.map((tab, index) => (
          <li
            key={tab.label}
            className={`-mb-px mr-1 last:mr-0 py-2 px-1 font-medium text-sm ${index === activeTab ? 'border-l-4 border-teal-500' : 'border-transparent'}`}
            onClick={() => setActiveTab(index)}
          >
            {tab.label}
          </li>
        ))}
      </ul>
      <div className="py-3">
        {tabs[activeTab].content}
      </div>
    </div>
  );
};

export default Tabs;
