import React from 'react';

type Props = { 
  title: string
  keywords: string
  description: string
}

const Meta = ({ title, keywords, description }: Props) => {
  return (
    <head>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta charSet='utf-8' />
      <link
					href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700;800&display=swap"
					rel="stylesheet"
				/>
      <link rel='icon' href='/favicon.ico' />
      <title>{title}</title>
    </head>
  )
}

Meta.defaultProps = {
  title: 'Mijn Maaltijd',
  keywords: 'recepten, koken, recipes, recipe, maaltijd, student, studenten, studentmeals, mijn maaltijd, maaltijd',
  description: 'Heerlijke recepten voor iedereen',
}

export default Meta