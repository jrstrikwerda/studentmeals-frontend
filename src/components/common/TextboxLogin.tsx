import React from "react";
interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
  lableText?: string;
  error?: string;
  children?: React.ReactNode;
}

const TextBox = React.forwardRef<HTMLInputElement, IProps>(
  ({ className, children, lableText, type = "text", error, ...props }, ref) => {
    return (
      <div className={className + " relative"}>
        {lableText && (
          <label
            className="block text-yellow-light  mb-2 text-sm xl:text-base font-normal"
            htmlFor="txt"
          >
            {lableText}
          </label>
        )}
        <div className="flex items-stretch">
          <input
            id="txt"
            autoComplete="off"
            className={`bg-pearl border border-tertiary-dark disabled:border-tertiary w-full outline-none py-2 px-2 transition-all text-green text-sm xl:text-base focus:shadow focus:shadow-chili-dark placeholder-green-light placeholder:text-sm
              ${error && "border-red-500 border  animate-shake"} ${
              children ? "rounded-r-md" : "rounded-md"
            }`}
            {...props}
            ref={ref}
            type={type}
          ></input>

          <div className="flex">{children}</div>
        </div>
        {error && (
          <p className="text-tomato text-right animate-shake">{error}</p>
        )}
      </div>
    );
  }
);

TextBox.displayName = "TextBox";
export default TextBox;
