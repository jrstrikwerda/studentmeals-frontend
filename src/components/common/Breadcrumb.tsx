import Link from 'next/link';

interface BreadcrumbItem {
  label: string;
  href: string;
}

interface BreadcrumbProps {
  items: BreadcrumbItem[];
  className?: string;
}

const Breadcrumb: React.FunctionComponent<BreadcrumbProps> = ({ items, className }) => {
  return (
    <nav className={`flex ${className}`} aria-label="breadcrumb">
      <ol className="list-none p-0 inline-flex">
        {items.map((item, index) => (
          <li key={item.label} className="flex items-center">
            {index < items.length - 1 ? (
              <Link href={item.href} className="text-sm font-medium text-dark hover:text-primary-dark transition duration-150 ease-in-out">
                  {item.label}
              </Link>
            ) : (
              <span className="text-sm font-medium text-primary-dark">{item.label}</span>
            )}
            {index < items.length - 1 && (
              <svg className="flex-shrink-0 mx-2 h-5 w-5 text-dark fill-dark" viewBox="0 0 20 20">
                <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
              </svg>
            )}
          </li>
        ))}
      </ol>
    </nav>
  );
};

export default Breadcrumb;