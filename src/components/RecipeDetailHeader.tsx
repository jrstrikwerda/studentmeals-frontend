import Image from "next/image";
import { RecipeIngredient, Recipe } from "../api/types";
import StarRating from "./common/StarRating";

const RecipeDetailHeader: React.FC<{ recipe: Recipe }> = ({ recipe }) => {
  const {
    image,
    description,
    name,
    cookTime = "30 min",
    servings = 4,
    rating = 3.5,
    numberRatings = 11,
    categories = ["italiaans", "vlees", "ovengerecht"],
  } = recipe;

  return (
    <div className="2xl:flex 2xl:flex-col 2xl:items-center">
      <div className="relative flex flex-wrap items-center justify-center content-end mb-48 lg:mb-32 w-full max-w-125vw min-h-40vh md:min-h-50vh 2xl:container">
        <Image
          src={image}
          alt={name}
          className="object-cover mb-5 brightness-80 object-center  2xl:rounded-xl"
          loading="lazy"
          fill
        />
        <div className="container mx-auto px-4 lg:px-0 -mb-48 lg:-mb-32 pt-48 lg:pt-32 lg:bg-pearl">
          <div className="bg-white p-6 rounded-xl bg-opacity-45 backdrop-filter backdrop-blur-md shadow-lg relative lg:max-w-xl inset-0 lg:mx-8 " >
            <div className="flex justify-between">
              <h1 className="text-4xl leading-normal font-extrabold mb-6 text-green shadow-light-dark text-shadow-sm">
                {name}
              </h1>
                <div className="focus:outline-none beat ml-4">
                  <svg
                    className="w-10 h-10 hover:animate-beat"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      className="w-6 h-6 text-tomato tertiary fill-transparent hover:fill-tomato"
                      d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"
                      fill="currentColor"
                      stroke="currentColor"
                      strokeWidth="2"
                    />
                  </svg>
              </div>
            </div>
            <p className="text-green text-shadow-sm shadow-light-darker text-lg mb-6">
              {description}
            </p>
            <div className="mb-6 flex gap-4 justify-around px-3 flex-wrap font-medium shadow-light-darker text-shadow-sm">
              <p className="text-primary-darker flex gap-2 items-center">
                {" "}
                <svg
                  width={20}
                  height={20}
                  className=" fill-primary-darker"
                  viewBox="0 0 32 32"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    xmlns="http://www.w3.org/2000/svg"
                    d="M16 0.75c-8.422 0-15.25 6.828-15.25 15.25s6.828 15.25 15.25 15.25c8.422 0 15.25-6.828 15.25-15.25v0c-0.010-8.418-6.832-15.24-15.249-15.25h-0.001zM16 28.75c-7.042 0-12.75-5.708-12.75-12.75s5.708-12.75 12.75-12.75c7.042 0 12.75 5.708 12.75 12.75v0c-0.008 7.038-5.712 12.742-12.749 12.75h-0.001zM17.25 15.482v-9.482c0-0.69-0.56-1.25-1.25-1.25s-1.25 0.56-1.25 1.25v0 10c0 0.345 0.14 0.658 0.366 0.884l3.999 4.001c0.226 0.226 0.539 0.366 0.884 0.366 0.691 0 1.251-0.56 1.251-1.251 0-0.345-0.14-0.658-0.366-0.884l0 0z"
                  />
                </svg>
                {cookTime} min
              </p>
              <p className="text-primary-darker flex gap-2 items-center">
                {" "}
                <svg
                  width={22}
                  height={22}
                  className=" fill-primary-darker"
                  viewBox="0 0 32 32"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    xmlns="http://www.w3.org/2000/svg"
                    d="M27.139,15H26V14a6.007,6.007,0,0,0-6-6V6a1,1,0,0,0-1-1H13a1,1,0,0,0-1,1V8a6.007,6.007,0,0,0-6,6v1H4.861A2.577,2.577,0,0,0,2.32,18l.474,2.848A2.569,2.569,0,0,0,5.335,23H6a5.006,5.006,0,0,0,5,5H21a5.006,5.006,0,0,0,5-5h.665a2.569,2.569,0,0,0,2.542-2.153L29.68,18a2.577,2.577,0,0,0-2.542-3ZM6,21H5.335a.576.576,0,0,1-.569-.481l-.474-2.847A.577.577,0,0,1,4.861,17H6ZM14,7h4V8H14Zm-2,3h8a4.007,4.007,0,0,1,3.874,3H8.126A4.007,4.007,0,0,1,12,10Zm12,6v7a3,3,0,0,1-3,3H11a3,3,0,0,1-3-3V15H24Zm3.233,4.518a.576.576,0,0,1-.569.482H26V17h1.139a.577.577,0,0,1,.569.672Z"
                  />
                </svg>
                Hoofdgerecht
              </p>
              <StarRating rating={rating} numberRatings={numberRatings} />
            </div>
            <div className="mb-4">
              <ul className="flex flex-row list-none gap-2 justify-center flex-wrap">
                {categories.map((category: string) => (
                  <li
                    className="bg-yellow-light text-dark text-base border-tertiary rounded-md px-1 uppercase"
                    key={category}
                  >
                    {category}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RecipeDetailHeader;
