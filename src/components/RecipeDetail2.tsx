"use client";

import Image from "next/image";
import { useState } from "react";
import { RecipeIngredient, Recipe, RecipeStep } from "../api/types";
import Button from "./common/Button";
import RecipeDetailHeader from "./RecipeDetailHeader";
import { AiOutlinePrinter, AiOutlineSave } from "react-icons/ai";
import {
  IoLogoFacebook,
  IoLogoPinterest,
  IoLogoWhatsapp,
} from "react-icons/io";
import { IoMail } from "react-icons/io5";

const RecipeDetail: React.FC<{ recipe: Recipe }> = ({ recipe }) => {
  const {
    image,
    description,
    name,
    recipeIngredients,
    recipeSteps,
    additional_images,
    cookTime = "30 min",
    servings = 4,
    rating = 3.5,
    numberRatings = 11,
    categories = ["italiaans", "vlees", "ovengerecht"],
  } = recipe;

  const recipeStepsDefault = [
    {
      order: 1,
      instruction: "De eerste instructies",
      recipe: {} as Recipe,
    },
    {
      instruction: "Nog wat meer instructies",
      order: 2,
      image: "mozarella",
      recipe: {} as Recipe,
    },
    {
      instruction:
        "Deze derde set aan instructies is een stukje langer. We typen nog een stukje door. Hier moet je de tomatensaus en de italiaanse kruiden toevoegen.",
      order: 3,
      recipe: {} as Recipe,
    },
    {
      instruction:
        "En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd",
      order: 4,
      recipe: {} as Recipe,
    },
    {
      instruction:
        "En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd",
      order: 5,
      recipe: {} as Recipe,
    },
    {
      instruction:
        "En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd",
      order: 6,
      recipe: {} as Recipe,
    },
    {
      instruction:
        "Deze zevende set aan instructies is een stukje langer. We typen nog een stukje door. Hier moet je de tomatensaus en de italiaanse kruiden toevoegen. En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd En nu een erg lange set aan instructies. LALALA Lorhfjksdhfdh hfdkhf hjkfdsh hfkdshjk fjhdkjs hfjk hfdkshj fhdjks hfkjd",
      order: 7,
      recipe: {} as Recipe,
    },
  ];

  const [numServings, setNumServings] = useState(servings);

  const handleIncrement = () => {
    setNumServings(numServings + 1);
  };

  const handleDecrement = () => {
    if (numServings > 1) {
      setNumServings(numServings - 1);
    }
  };

  return (
    <div>
      <RecipeDetailHeader recipe={recipe}></RecipeDetailHeader>
      <div className="container mx-auto p-4 lg:p-0 lg:bg-pearl">
        <div className="lg:flex gap-8   lg:px-8 lg:py-12  rounded-xl " >
          <div className="mb-4 lg:w-1/3 bg-white text-green-light rounded-xl p-6 sticky lg:self-start lg:top-8 pb-8 shadow-lg bg-opacity-45 backdrop-filter backdrop-blur-md" >
            <h2 className="text-2xl font-bold mb-2 text-green">
              Ingrediënten
            </h2>
            <div className="flex gap-3 my-4">
              <button
                className="text-lg font-medium rounded-md bg-yellow hover:bg-yellow-light px-2 text-dark"
                onClick={handleDecrement}
              >
                -
              </button>
              <div className="text-lg font-medium text-dark">
                {numServings} personen
              </div>
              <button
                className="text-lg font-medium rounded-md bg-yellow hover:bg-yellow-light px-2 text-dark"
                onClick={handleIncrement}
              >
                +
              </button>
            </div>
            <ul className="list-none mb-4">
              {recipeIngredients?.map((recipeIngredient: RecipeIngredient) => (
                <li
                  className="text-lg lh pt-3 pb-2 border-b border-green"
                  key={recipeIngredient?.ingredient?.name}
                >
                  {(recipeIngredient?.amount / servings) * numServings}{" "}
                  <span className="pr-1">{recipeIngredient?.measurement}</span>{" "}
                  <span className="capitalize text-dark">
                    {recipeIngredient?.ingredient?.name}
                  </span>
                  <span className="text-md text-dark">
                    {recipeIngredient?.usage
                      ? `, ${recipeIngredient?.usage}`
                      : ""}
                  </span>
                </li>
              ))}
            </ul>
            <div className="flex flex-wrap gap-4 justify-between mb-4">
              <Button
                text={
                  <div className="flex gap-2 items-center">
                    <div className="text-lg">
                      <AiOutlinePrinter />
                    </div>
                    Recept afdrukken
                  </div>
                }
                url="#"
                className="bg-yellow text-dark hover:bg-yellow-light w-full xs:w-max"
              />
              <Button
                text={
                  <div className="flex gap-2 items-center">
                    <div className="text-lg">
                      <AiOutlineSave />
                    </div>
                    Recept bewaren
                  </div>
                }
                url="#"
                className="bg-yellow text-dark hover:bg-yellow-light w-full xs:w-max"
              />
            </div>
            <div className="flex gap-3 flex-wrap justify-center text-green ">
              <span className="text-base text-dark">Delen via</span>
              <div className="flex flex-wrap gap-3">
                <IoMail className="hover:fill-chili-dark ease-in duration-300" />
                <IoLogoWhatsapp className="hover:fill-chili-dark ease-in duration-300" />
                <IoLogoFacebook className="hover:fill-chili-dark ease-in duration-300" />
                <IoLogoPinterest className="hover:fill-chili-dark ease-in duration-300" />
              </div>
            </div>
          </div>
          <div className="mb-4 lg:w-2/3 py-6 flex flex-col rounded-xl">
            <h2 className="text-2xl font-bold mb-4 text-green">
              Bereidingswijze
            </h2>

            {recipeSteps?.length
              ? recipeSteps.map((recipeStep: RecipeStep, index: number) => (
                  <div
                    className="flex gap-x-4 md:gap-x-6 lg:gap-x-10 text-dark aos-init aos-animate"
                    data-aos="zoom-in-up"
                    key={index}
                  >
                    <div
                      className={`flex relative ${
                        recipeSteps.length - 1 === index
                          ? undefined
                          : "after:absolute after:border-l after:border-dashed after:border-black after:left-1/2 after:-z-1 after:-translate-x-1/2 after:top-12 after:bottom-2"
                      }`}
                    >
                      <span className="flex items-center justify-center text-center rounded-full bg-yellow-light border-solid w-10 h-10 flex-shrink-0 font-heading text-lg font-semibold">
                        {index + 1}
                      </span>
                    </div>
                    <div className="flex flex-col gap-y-1 pb-12">
                      <span className="text-lg">{recipeStep.instruction}</span>
                    </div>
                  </div>
                ))
              : recipeStepsDefault.map(
                  (recipeStep: RecipeStep, index: number) => (
                    <div
                      className="flex gap-x-4 md:gap-x-6 lg:gap-x-10 text-dark aos-init aos-animate"
                      data-aos="zoom-in-up"
                      key={index}
                    >
                      <div
                        className={`flex relative ${
                          recipeStepsDefault.length - 1 === index
                            ? undefined
                            : "after:absolute after:border-l after:border-dashed after:border-black after:left-1/2 after:-z-1 after:-translate-x-1/2 after:top-12 after:bottom-2"
                        }`}
                      >
                        <span className="flex items-center justify-center text-center rounded-full  bg-yellow-light border-solid w-10 h-10 flex-shrink-0 font-heading text-lg font-semibold">
                          {index + 1}
                        </span>
                      </div>
                      <div className="flex flex-col gap-y-1 pb-12">
                        <span className="text-lg">
                          {recipeStep.instruction}
                        </span>
                      </div>
                    </div>
                  )
                )}
          </div>
        </div>
        {additional_images && additional_images.length > 0 && (
          <div className="mb-4">
            <h2 className="text-xl font-bold mb-2">Additional Images</h2>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
              {additional_images.map((img: string) => (
                <Image
                  src={img}
                  alt={name}
                  className="w-full"
                  key={img}
                  width={600}
                  height={400}
                  loading="lazy"
                />
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default RecipeDetail;
