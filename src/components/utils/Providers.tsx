"use client";
import React, { useState } from "react";
import { SessionProvider } from "next-auth/react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { Session } from "next-auth";
import RefreshTokenHandler from "../RefreshTokenHandler";

const queryClient = new QueryClient();

type Props = { children: React.ReactNode; session: Session };

export default function Providers({ children, session }: Props) {
  const [interval, setInterval] = useState(7200);
  return (
    <SessionProvider session={session} refetchInterval={interval}>
      <QueryClientProvider client={queryClient}>
        <RefreshTokenHandler setInterval={setInterval} />
        {children}
      </QueryClientProvider>
    </SessionProvider>
  );
}
