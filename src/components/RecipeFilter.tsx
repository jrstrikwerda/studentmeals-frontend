import React, { FormEvent, useState } from "react";
import { useRouter } from 'next/navigation';

const RecipeFilter: React.FC = () => {
  const router = useRouter();
  const [cuisine, setCuisine] = useState("");
  const [dietaryRestriction, setDietaryRestriction] = useState("");

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    router.push(
      `/recipes?cuisine=${cuisine}&dietaryRestriction=${dietaryRestriction}`
    );
  };

  return (
    <form onSubmit={handleSubmit} className="w-full max-w-lg mt-4 mx-auto">
      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            Cuisine
          </label>
          <div className="relative rounded-md shadow-sm">
            <select
              value={cuisine}
              onChange={(event) => setCuisine(event.target.value)}
              className="block form-input py-2 px-3 leading-tight focus:outline-none focus:shadow-outline-blue"
            >
              <option value="">All</option>
              <option value="italian">Italian</option>
              <option value="mexican">Mexican</option>
              <option value="chinese">Chinese</option>
              <option value="american">American</option>
            </select>
          </div>
        </div>
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            Dietary Restriction
          </label>
          <div className="relative rounded-md shadow-sm">
            <select
              value={dietaryRestriction}
              onChange={(event) => setDietaryRestriction(event.target.value)}
              className="block form-input py-2 px-3 leading-tight focus:outline-none focus:shadow-outline-blue"
            >
              <option value="">None</option>
              <option value="vegetarian">Vegetarian</option>
              <option value="vegan">Vegan</option>
              <option value="gluten-free">Gluten-free</option>
            </select>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-end">
        <button
          type="submit"
          className="inline-block align-middle text-center select-none border font-bold whitespace-no-wrap py-2 px-4 rounded-full text-base leading-normal no-underline text-gray-100
          bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:border-indigo-800 focus:shadow-outline-indigo active:bg-indigo-800"
        >
          Filter Recipes
        </button>
      </div>
    </form>
  );
};

export default RecipeFilter;
