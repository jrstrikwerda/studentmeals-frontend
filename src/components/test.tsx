"use client";
import React from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import navbarStyles from "../styles/components/Nav.module.scss";

const Nav = () => {
  const pathname = usePathname();
  function toggleMobileMenu(dontOpen = false) {
    const menu = document.querySelector("#menu");
    const mobile_bar = document.querySelector("#mobile-bar");
    if (dontOpen) {
      if (menu?.classList.contains(`${navbarStyles.active2}`)) {
        menu?.classList.toggle(`${navbarStyles.active2}`);
        mobile_bar?.classList.toggle(`${navbarStyles.active3}`);
      }
    } else {
      menu?.classList.toggle(`${navbarStyles.active2}`);
      mobile_bar?.classList.toggle(`${navbarStyles.active3}`);
    }
  }
  const user = {
    data: {
      name: "Jorrit",
    },
  };
  const isAuthenticated = true;
  const authLinks = (
    <ul className="navbar-nav ml-auto mt-2 mt-lg-0 ">
      <li className="nav-item mr-3">
        <Link className="nav-link" href="/admin">
          Admin Control
        </Link>
      </li>
      <span className="navbar-text mr-3">
        <strong>{user ? `Logged in as ${user.data.name}` : ""}</strong>
      </span>
      <li className="nav-item">
        <button
          // onClick={this.props.logout}
          className="nav-link btn btn-danger btn-sm text-light"
        >
          Logout
        </button>
      </li>
    </ul>
  );

  const guestLinks = (
    <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
      <li className="nav-item">
        <Link href="/register" className="nav-link">
          Register
        </Link>
      </li>
      <li className="nav-item">
        <Link href="/login" className="nav-link">
          Login
        </Link>
      </li>
    </ul>
  );
  return (
    <div className={navbarStyles.navbar_wrapper}>
      <nav className={navbarStyles.navbar + " screenwidth"} id="navbar">
        {/* Brand/Logo */}
        <div className={navbarStyles.logo + " drop_shadow"}>
          <Link href="/" onClick={toggleMobileMenu}>
            Mijn Maaltijd
          </Link>
        </div>
        {/* Menu  */}
        <ul className={navbarStyles.menu} id="menu">
          {/* {isAuthenticated ? authLinks : guestLinks} */}
          <li className="nav-item">
            <Link
              className={
                (pathname == "/" ? navbarStyles.active : "") + " drop_shadow"
              }
              onClick={toggleMobileMenu}
              href="/about"
            >
              About
            </Link>
          </li>
          <li className="nav-item">
            <Link
              className={
                (pathname == "/info" ? navbarStyles.active : "") +
                " drop_shadow"
              }
              onClick={toggleMobileMenu}
              href="/info"
            >
              Info
            </Link>
          </li>
          <li className="nav-item">
            <Link
              className={
                (pathname == "/recipes" ? navbarStyles.active : "") +
                " drop_shadow"
              }
              onClick={toggleMobileMenu}
              href="/recipes"
            >
              Recipes
            </Link>
          </li>
        </ul>
        {/* MobileMenu */}
        <div
          className={navbarStyles.mobile_bar}
          id="mobile-bar"
          onClick={() => toggleMobileMenu(false)}
        >
          <span className="drop_shadow"></span>
          <span className="drop_shadow"></span>
          <span className="drop_shadow"></span>
        </div>
      </nav>
    </div>
  );
  // return (
  //   <nav className={navStyles.nav}>
  //     <ul>
  //       <li>
  //         <Link href="/">Home</Link>
  //       </li>
  //       <li>
  //         <Link href="/about">About</Link>
  //       </li>
  //     </ul>
  //   </nav>
  // );
};

export default Nav;
