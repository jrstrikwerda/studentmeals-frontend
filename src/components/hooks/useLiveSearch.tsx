import { useState, useEffect } from "react";
import { Recipe } from "../../api/types";
import {
  FetchError,
  FetchResult,
  searchRecipes,
} from "../../api/recipes/fetchFunctions";

interface LiveSearchReturnType {
  searchQuery: string;
  setSearchQuery: React.Dispatch<React.SetStateAction<string>>;
  searchStarted: boolean;
  results: Recipe[];
  error: Error | null;
  loading: boolean;
  handleFocus: () => void;
  handleBlur: () => void;
  showResults: boolean;
}

const useLiveSearch = (): LiveSearchReturnType => {
  const [searchQuery, setSearchQuery] = useState("");
  const [results, setResults] = useState<Recipe[]>([]);
  const [error, setError] = useState<FetchError | null>(null);
  const [loading, setLoading] = useState(false);
  const [showResults, setShowResults] = useState(false);
  const [searchStarted, setSearchStarted] = useState(false);

  const handleFocus = () => {
    if (results.length > 0) {
      setShowResults(true);
    }
  };

  const handleBlur = () => {
    const timer = setTimeout(() => {
      setShowResults(false);
    }, 200); // set the delay time here
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setSearchStarted(!!searchQuery);
    }, 500); // set the delay time here

    return () => clearTimeout(timer);
  }, [searchQuery]);

  useEffect(() => {
    let cancelQuery = false;
    let timeoutId: NodeJS.Timeout;

    const delayedSearch = () => {
      setLoading(true);
      searchRecipes(searchQuery)
        .then((result: FetchResult<Recipe[]>) => {
          if (!cancelQuery) {
            if (result.error) {
              setError(result.error);
            } else {
              setResults(result.data || []);
              setShowResults(true);
            }
            setLoading(false);
          }
        })
        .catch((err) => {
          if (!cancelQuery) {
            setError(err);
            setLoading(false);
          }
        });
    };

    if (searchQuery) {
      timeoutId = setTimeout(delayedSearch, 500);
    } else {
      setShowResults(false);
      setResults([]);
    }

    return () => {
      clearTimeout(timeoutId);
      cancelQuery = true;
    };
  }, [searchQuery]);

  return {
    searchQuery,
    setSearchQuery,
    searchStarted,
    results,
    error,
    loading,
    handleFocus,
    handleBlur,
    showResults,
  };
};

export default useLiveSearch;
