import { signOut, useSession } from "next-auth/react";
import { usePathname, useRouter } from "next/navigation";
import { useEffect, useState } from "react";

export default function useAuth(shouldRedirect: boolean, allowedRoles?: string[]): boolean {
  const { data: session } = useSession();
  const router = useRouter();
  const pathname = usePathname();
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    if (session?.error === "RefreshAccessTokenError") {
      signOut({ callbackUrl: "/account/inloggen", redirect: shouldRedirect });
    }

    if (session === null ) {
      if (pathname !== "/account/inloggen" &&  shouldRedirect) {
        router.push("/account/inloggen");
      }
      setIsAuthenticated(false);
    } else if (session !== undefined) {
        const userRole = session.user.role;
        if (allowedRoles && userRole && !allowedRoles.includes(userRole)) {
          router.push("/");
        //   signOut({ callbackUrl: "/account/inloggen", redirect: shouldRedirect });
        } else if (pathname === "/account/inloggen") {
          router.push("/");
        }
        setIsAuthenticated(true);
      }
  }, [allowedRoles, pathname, router, session, shouldRedirect]);

  return isAuthenticated;
}
