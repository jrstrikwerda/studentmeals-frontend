import RecipeItem from "./RecipeItem";
import recipeStyles from "../styles/components/Recipe.module.scss";
import React from 'react';

import { Recipe } from "../api/types";

type Props = {
  recipes: Recipe[];
};

const RecipeList = ({ recipes }: Props)=> {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-3 gap-8 row-gap-8 my-8">
      {recipes.map((recipe) => {
        return (
          <RecipeItem recipe={recipe} key={recipe._id} />
        );
      })}
    </div>
  );
};

export default RecipeList;
