import axios from "axios";
import { api } from "../../../config";
import { UserRegister } from "../types";

interface SignupData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

interface SignupResult {
  user?: UserRegister;
  error?: string;
}

export const signup = async ({
  firstName,
  lastName,
  email,
  password,
}: SignupData): Promise<SignupResult> => {
  try {
    // Send a POST request to your Nest.js backend API to create a new user
    const response = await axios.post<UserRegister>(api + "/auth/register", {
      firstName,
      lastName,
      username: email,
      password,
    });

    return { user: response.data }; // Return the created user
  } catch (error: any) {
    return { error: error.message }; // Return the error message
  }
};
