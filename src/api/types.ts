import { String } from "aws-sdk/clients/apigateway";
import NextAuth from "next-auth";

export type Recipe = {
  _id: string;
  name: string;
  description: string;
  image: string;
  cookTime: string;
  creationDate: string;
  updateDate: string;
  recipeIngredients: RecipeIngredient[];
  categories: string[];
  rating: number;
  numberRatings: number;
  additional_images: string[];
  servings: number;
  recipeSteps: RecipeStep[];

  // categories: Category[]
};

export type RecipeIngredient = {
  amount: number;
  measurement: string;
  ingredient: Ingredient;
  recipe: Recipe;
  usage: string;
};

export type RecipeStep = {
  instruction: string;
  order: number;
  image?: string;
  recipe: Recipe;
};

export type Ingredient = {
  name: string;
  description?: string;
  recipes: Recipe[];
};

export interface RecipeState {
  recipes: Recipe[];
  isLoading: boolean;
  error: Error | null;
}

declare module "next-auth" {
  /**
   * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
   */
  interface Session {
    user: User;
    accessToken: string;
    accessTokenExpiration: number;
    error?: string;
  }
}

export interface User {
  name?: string;
  role?: string;
  username?: string;
  _id?: string;
}

export interface UserRegister {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

// export type Movie = {
//     backdrop_path: string;
//     id: number;
//     original_title: string;
//     overview: string;
//     popularity: number;
//     poster_path: string;
//     title: string;
//     vote_average: number;
//     vote_count: number;
//     budget: number;
//     runtime: number;
//     revenue: number;
//     release_date: string;
//   };

//   export type Movies = {
//     page: number;
//     results: Movie[];
//     total_pages: number;
//     total_results: number;
//   };

//   export type Cast = {
//     character: string;
//     credit_id: string;
//     name: string;
//     profile_path: string;
//   };

//   export type Crew = {
//     job: string;
//     name: string;
//     credit_id: number;
//   };

//   export type Credits = {
//     id: number;
//     cast: Cast[];
//     crew: Crew[];
//   };
