import { api } from "../../../config";
import { Recipe } from "../types";

export interface FetchError extends Error {
  status: number;
}

export const basicFetch = async (endpoint: string, init?: RequestInit) => {
  const response = await fetch(`${api}${endpoint}`, init);

  if (!response.ok) {
    const error: FetchError = new Error(response.statusText) as FetchError;
    error.status = response.status;
    throw error;
  }

  return response;
};

export async function getRecipes(): Promise<Recipe[]> {
  const res = await basicFetch(`/recipes`, { next: { revalidate: 3600 } });
  const recipes = (await res.json()) as Recipe[];
  return recipes;
}

export async function getRecipe(recipeId: string): Promise<Recipe> {
  const res = await basicFetch(`/recipes/findone/${recipeId}`);
  const recipe = (await res.json()) as Recipe;
  return recipe;
}

export interface FetchResult<T> {
  data?: T;
  error?: FetchError;
}

export async function searchRecipes(
  searchQuery: string
): Promise<FetchResult<Recipe[]>> {
  try {
    const params = new URLSearchParams({ query: searchQuery });
    const res = await basicFetch(`/recipes/search?${params.toString()}`);
    const recipes = (await res.json()) as Recipe[];
    return { data: recipes };
  } catch (error) {
    return { error: error as FetchError };
  }
}

// // Fetch functions

// // Get all recipes
// export const fetchRecipes = async (): Promise<Recipe[]> => {
//   return await basicFetch<Recipe[]>(`/recipes`);
// };

// // Get recipe by id
// export const fetchRecipe = async (recipeId: string): Promise<Recipe> => {
//   return await basicFetch<Recipe>(`/recipes/findone/${recipeId}`);
// };

// Search recupe by searchQuery
// export const searchRecipe = async (searchQuery: string) => {
//   return fetch(`${api}/recipes/search?query=${searchQuery}`).then((res) => {
//     return res.json();
//   });
// };
