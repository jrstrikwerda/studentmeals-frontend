import { getServerSession } from "next-auth";
import { authOptions } from "../pages/api/auth/[...nextauth]";

// export async function getSession(cookie: string): Promise<Session> {
//   console.log("Getting session");
//   const response = await fetch(`${LOCAL_AUTH_URL}/api/auth/session`, {
//     headers: {
//       cookie,
//     },
//   });

//   const session = await response.json();
//   console.log({ session });
//   console.log("=============")

//   return Object.keys(session).length > 0 ? session : null;
// }

// Getting the session in Next13 app/ directory
// https://next-auth.js.org/configuration/nextjs#in-app-directory
export async function getSession() {
  const session = await getServerSession(authOptions);
  return session;
}

export async function getCurrentUser() {
  const session = await getSession();
  return session?.user;
}
