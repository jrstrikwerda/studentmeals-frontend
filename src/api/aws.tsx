import AWS from "aws-sdk";

const s3 = new AWS.S3({
  accessKeyId: process.env.STUDENTMEALS_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.STUDENTMEALS_AWS_SECRET_ACCESS_KEY,
  region: "eu-central-1",
});

interface S3Error {
  message: string;
}

export const getSignedUrl = async (key: string): Promise<string> => {
  if (!key) {
    throw new Error("Missing object key");
  }
  const bucketName = process.env.STUDENTMEALS_AWS_BUCKET_NAME || "studentmeals";
  if (!bucketName) {
    throw new Error("Missing bucket name");
  }
  try {
    const params = {
      Bucket: bucketName,
      Key: key,
      Expires: 3600,
    };
    const signedUrl = await s3.getSignedUrlPromise("getObject", params);
    return signedUrl;
  } catch (error) {
    if (isS3Error(error)) {
      throw new Error(`Error generating signed URL: ${error.message}`);
    } else {
      throw new Error(`Error generating signed URL.`);
    }
  }

  function isS3Error(error: unknown): error is S3Error {
    return typeof (error as S3Error).message === "string";
  }
};
