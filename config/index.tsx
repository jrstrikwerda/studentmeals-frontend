const dev = process.env.NODE_ENV !== "production";
// Sizes: w300, w780, w1280, original
const BACKDROP_SIZE: string = "w1280";
// w92, w154, w185, w342, w500, w780, original
const POSTER_SIZE: string = "w780";
const api = dev ? "http://localhost:3000" : "https://studentmeals-backend.onrender.com";
export { dev, api, POSTER_SIZE, BACKDROP_SIZE };
