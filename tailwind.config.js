/** @type {import('tailwindcss').Config} */
const plugin = require("tailwindcss/plugin");

module.exports = {
  content: [
    "./src/app/**/*.{js,ts,jsx,tsx}",
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  mode: "jit",
  media: false,
  theme: {
    fontFamily: {
      poppins: ["Poppins", "sans-serif"],
    },
    // colors: {
    //   transparent: "transparent",
    //   current: "currentColor",
    //   background: "#f5f5f5", // White smoke

    //   primary: '#8e9189',
    //   secondary: '#494449',
    //   tertiary: '#857f74',
    //   light: '#eeefeb',
    //   accent: '#ddeac1',
    //   'primary-light': '#b9c0b5',
    //   'primary-lighter': '#d6d9d4',
    //   'primary-dark': '#6c746f',
    //   'primary-darker': '#565a56',
    //   'secondary-light': '#6e6e6e',
    //   'secondary-lighter': '#878787',
    //   'secondary-dark': '#2f2f2f',
    //   'secondary-darker': '#1c1c1c',
    //   'tertiary-light': '#a5a5a5',
    //   'tertiary-lighter': '#c4c4c4',
    //   'tertiary-dark': '#696969',
    //   'tertiary-darker': '#474747',
    //   'light-light': '#f9f9f9',
    //   'light-lighter': '#fcfcfc',
    //   'light-dark': '#c7c7c7',
    //   'light-darker': '#9d9d9d',
    //   'accent-light': '#e1e3be',
    //   'accent-lighter': '#e9eac6',
    //   'accent-dark': '#b5b792',
    //   'accent-darker': '#91936e',
    //   dark: "#080808", // Smokey black
    //   active: "#abac7f", // Grullo
    // },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      background: "#f5f5f5",

      primary: "#2B7546",
      secondary: "#422D09",
      tertiary: "#AC7C36",
      light: "#E3E2DF",
      accent: "#F08E88",
      "accent-text": "#ddeac1",
      "primary-light": "#468F5E",
      "primary-lighter": "#61AA77",
      "primary-lightest": "#CEFFE3",
      "primary-dark": "#095C2F",
      "primary-darker": "#004419",
      "primary-darkest": "#002D00",
      "secondary-light": "#6F4D18",
      "secondary-lighter": "#946F2A",
      "secondary-dark": "#3D1A05",
      "secondary-darker": "#2A1302",
      "tertiary-light": "#C4A246",
      "tertiary-lighter": "#DBC66D",
      "tertiary-dark": "#8B5D25",
      "tertiary-darker": "#63421B",
      "light-light": "#F3F3F0",
      "light-lighter": "#FAFAF7",
      "light-dark": "#C9C9C6",
      "light-darker": "#AFAFAE",
      "accent-light": "#F7A4A4",
      "accent-lighter": "#FBC6C6",
      "accent-dark": "#D07E7E",
      "accent-darker": "#A65656",
      chili: "#E32227",
      "chili-light": "#E84B4F",
      "chili-lighter": "#ED7377",
      "chili-lightest": "#F29C9E",
      "chili-dark": "#BF181D",
      "chili-darker": "#961316",
      "chili-darkest": "#6D0E10",
      tomato: "#9C2813",
      "tomato-inner": "#b9261d",
      dark: "#2D3538",
      active: "#abac7f",
      white: "#FFFFFF",
      "faded-primary": "#AED0B6",
      yellow: "#FFCB44",
      "yellow-light": "#FFDC85",
      pearl: "#FFF5E7",
      green: "#074F48",
      "green-light": "#487B6F",
    },
    extend: {
      spacing: {
        128: "40rem",
      },
      opacity: {
        35: ".35",
      },
      textShadow: {
        sm: "0 1px 2px var(--tw-shadow-color)",
        DEFAULT: "0 2px 4px var(--tw-shadow-color)",
        lg: "0 8px 16px var(--tw-shadow-color)",
      },
      scale: {
        102: "1.02",
      },
      backdropFilter: {
        none: "none",
        blur: "blur(20px)",
      },
      height: {
        "55vh": "55vh",
        "44vh": "44vh",
        test: "26rem",
      },
      minHeight: {
        128: "40rem",
        "65vh": "65vh",
        "50vh": "50vh",
        "40vh": "40vh",
        "30vh": "30vh",
      },
      maxWidth: {
        "125vw": "125vw",
      },
      brightness: {
        60: ".6",
        65: ".65",
        70: ".7",
        75: ".75",
        80: ".8",
      },
      fontSize: {
        "2.5rem": "2.5rem",
      },

      keyframes: {
        beat: {
          "0%": { transform: "scale(1)" },
          "20%": { transform: "scale(1.2)" },
          "40%": { transform: "scale(1)" },
        },
      },
      animation: {
        beat: "beat .5s infinite",
      },
    },

    // gridTemplateColumns: {
    //   "auto-fill": "repeat(auto-fill, minmax(200px, 1fr))",
    // },
  },

  variants: {
    extend: {},
  },
  plugins: [
    require("tailwindcss"),
    require("autoprefixer"),
    require("tailwindcss/plugin"),
    require("tailwindcss-filters"),
    require("tailwind-scrollbar")({ nocompatible: true }),
    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          "text-shadow": (value) => ({
            textShadow: value,
          }),
        },
        { values: theme("textShadow") }
      );
    }),
  ],
};
