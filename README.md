# Next.js Mijn Maaltijd Project

Mijn Maaltijd Next.js project

### Run the development server:

```bash
npm run dev
```

### Build for production AND generate static website into "out" folder:

```bash
npm run build
```

### Run in production:

```bash
npm start
```

### API Routes:

GET /api/recipes
GET /api/recipes/:id