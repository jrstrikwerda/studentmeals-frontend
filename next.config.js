/** @type {import('next').NextConfig} */
const path = require("path");
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  basePath: "",
  images: {
    domains: [
      "flowbite.com",
      "studentmeals.s3.eu-central-1.amazonaws.com",
      "d31wbbiiznjm40.cloudfront.net",
    ],
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    });
    return config;
  }
};

module.exports = nextConfig;
